---
layout: post
title: "Week Four: Pt 2"
published: true
---

I really enjoyed these readings.

In the documentary "The Social Life of Urban Spaces" the idea that spaces create the choreography really tickled me. Last month, I was exposed to the idea that fundamentally *structure* causes *behavior*. I have been trying to think about all behaviors through this lens, from social interactions to chemistry. Studying behavior gives insight into the structure; and then tweaking structure effects changes in behavior.

The two places that came to mind when thinking about plazas were [Museumsquartier](https://en.wikipedia.org/wiki/Museumsquartier) in Vienna, and [Miradouro de Santa Catarina](https://goo.gl/maps/SAVN1rhmhY62) in Lisbon. Both of these had all the necessities for a lively urban space, community thrived in the space.

[![lisbon park](/assets/images/portugal_park_s.jpg)](/assets/images/portugal_park.jpg)

One thing I really connected with is giving people choice (even if they don't need it) to make them feel more powerful in a space.

I wonder how smartphones have changed the power of triangulation. Instead of looking at a performer or statue and talking about it with a stranger, I see many people take a video and send it to the internet. The few times that I've had a conversation with a stranger in a public place, it has been with older people or those on the fringe that choose to not be internet-connected.

The Counter (Mapping) Actions paper presents social co-research and mapping events held at UNC. It used many terms with political meanings that I did not know before, and I appreciate how they explained all of them with examples. They created maps through co-research to organize people that otherwise did not have a way to connect. Compared to the traditional role of maps to give states authority, this is subversive. I especially connected to the framing of creating maps to discover new ideas instead of codifying the status quo. I have looked at their work and presentations hoping to find actionable ways to do grassroots militant cartography, but have not had time to dive in.

The "Speculative infrastructures" post about social infrastructure on the margins also connects with the theme of structure causing behavior. City planners design cities for some people, and in the process (as with any design process) exclude others. Margins exist on all boundaries (social, geographical, ideological), and force innovation. The center is static, the margins are always shifting and to be there requires constant creativity.

These readings have made me question my static views of a city. Instead of being set in stone, urban spaces are perpetually being reinterpreted. We can guide this process with social research towards human goodness.

### References:
> William Whyte's The Social Life of Urban Spaces
>
> [Counter (Mapping) Actions: Mapping as Militant Research](https://www.acme-journal.org/index.php/acme/article/view/941)
>
> [‘Speculative infrastructures’ at the urban margins](http://africanurbanism.net/speculative-infrastructures-at-the-urban-margins/)
