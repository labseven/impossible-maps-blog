---
layout: post
title: "Turn Straight"
published: true
---

What if map directions conformed to your inalianable right to walk straight to your destination?

[![warped map](/assets/images/Bendy_OSM_directions_600x600.jpg)](/assets/images/Bendy_OSM_directions.png)

The map is warped to make the path to your destination straight. To use the map, simply bend the earth along your route and stroll without worrying about directions.

> See also: [HyperRogue](http://www.roguetemple.com/z/hyper/)
