---
layout: map
title: "Basic Web Map"
description: "My first webmap"
keywords: "week one, assignment, map"
published: true
---
<!-- Leaflet stylesheet -->
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ==" crossorigin="" />
<!-- Leaflet javascript  -->
<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js" integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log==" crossorigin=""></script>
<div>
  <div id="map"> </div>
  <script>
  var map = L.map('map').setView([49.362367, 15.384540], 18);

  L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png', {
      attribution: 'OpenStreetMap',
  }).addTo(map);

  var polygon = L.polygon([
    [49.36225,15.38408],
    [49.36247,15.38409],
    [49.36264,15.38453],
    [49.3626,15.38484],
    [49.36235,15.38537],
    [49.36221,15.38561],
    [49.36202,15.38556],
    [49.36187,15.38543],
    [49.36183,15.38515],
    [49.36196,15.38498],
    [49.36215,15.38462]
  ]).addTo(map);

  var rohozna = L.marker([49.362367, 15.384540]).addTo(map);
  rohozna.bindPopup("<b>Rohozná Summer Camp</b>").openPopup();
  </script>
  ars
</div>
