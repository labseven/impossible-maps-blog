---
layout: post
title: "Week Four: Pt 1"
published: true
---

The questions and themes in the HSTF focus group seem to lend themselves to mapping very well.

A common theme I see is the desire for events. They want events that connect the community on three levels; the youth, the neighborhood, and the broader Boston. I love events because they are a way to experiment without the burden of commitment. I wonder how counterpower maps can be used to plan events, and sustain the energy from an event into the future. Many of the questions and answers are seeking ways to foster connections within the Boston Latinx community.

I tried to get a sense for the neighborhood by looking at different representations: the HSTF map, satellite imagery, and street view. I still don't feel like I have a sense for the area until I walk around, sit in a public space, and get a feel for the vibes. How can a map excite me to go visit and learn about the neighborhood? To me it's 🍴 icons on google maps.

Very actionably, I noticed many small ways the HSTF map could be improved in its usability, giving it more power. I wonder what this map is used for currently, and how the highlighted businesses / organizations were selected. Maybe we can make a map with more immersive / compelling view into the culture in the quarter. How do local students interact with the area? What do retired adults do? What can we learn about the needs of the community by mapping their community this way?

This reminded me of a project in Prague, [Viadukt Kreativni](http://viaduktkreativni.cz/projekt/), which transformed a run-down street under a [viaduct](https://en.wikipedia.org/wiki/Viaduct) into a living place of culture. After renovating the street they kick-started a community by buildind a food stand; ping-pong tables, volleyball, and chess; hosting music events and film screenings; and they had plenty of movable seating. All of these were mentioned in *The Social Life of Urban Spaces* as important ingredients for a thriving space. The area has became extremely popular, and every afternoon it is full of people enjoying the atmosphere.

![viaduct](/assets/images/viaduct.jpg)
