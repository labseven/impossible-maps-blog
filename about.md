---
layout: page
title: About
permalink: /about/
---

My blog for [Impossible Maps](https://github.com/MimiOnuoha/Impossible-Maps) Independent Study with [Mimi Onuoha](https://github.com/MimiOnuoha), Spring 2019.

My other website is at [labseven.space](https://labseven.space).
